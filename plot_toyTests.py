# Script written by Rachel Hyneman, 2019
# Contact: rachel.hyneman@cern.ch

import numpy as np
import ROOT
import uproot
import csv
import sys
sys.path.append('inc/')
from utils import generateBackgroundWorkspace, generateSignalWorkspace, getInputXAxis, makeBinning, makeGPtemplate, define_background_functions, fluctuateTemplate, performSBFit, makeHistoFromArray

uproot.default_library = "np"

########### User Defined Options ##########

# These are all pulled from the inc/inputs.py file

import inputs as userin


###########################################
########### The Important Stuff ###########

def main():

	if( not hasattr(userin, 'templateHistoNames') ):
		print("ERROR! No template histogram names given (templateHistoNames). Nothing to smooth!")
		return

	templateHistoNames = userin.templateHistoNames
	nCategories = len(userin.templateHistoNames)

	categoryNames = ""
	if( hasattr(userin, 'categoryNames') ): categoryNames = userin.categoryNames
	if ( len(categoryNames) != nCategories ):
		print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
		categoryNames = templateHistoNames

	inTemplateFile = ROOT.TFile.Open(userin.inFileName,"READ")
	inToyTestFile = ROOT.TFile.Open(userin.outToyTestFileName,"READ")
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C")
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C")
	ROOT.SetAtlasStyle()


	for iCat in range(nCategories):

		inHistName = templateHistoNames[ iCat ]
		if( len(categoryNames) ): catName = categoryNames[ iCat ]
		else: catName = inHistName

		myBkgFunctions = define_background_functions()

		print( "Plotting Toy Results for Category: ", catName )

		gprHistoVals = []
		toyHistoVals = []

		### First get the actual toy histos+fits

		treeName = inHistName+"_ToyTests_gen"
		inFile = uproot.open( userin.outToyTestFileName )
		genTree = inFile[treeName]
		nToys = len( genTree['nBkgEvts'].array() )

		nBkgEvts = genTree['nBkgEvts'].array()[0]
		nSigEvts = genTree['nSigEvts'].array()[0]

		binEdges = np.array( genTree['binning'].array()[0] , dtype='d' )
		nBins = int( len( binEdges ) - 1 )
		myBins = ROOT.RooBinning(nBins,binEdges,"myBins")

		# Get the original template shape, or, if using analytic toys, the basis function

		origFile = ROOT.TFile.Open(userin.inFileName)
		origHisto = origFile.Get(inHistName)
		# If we need to, trim the input histogram
		gpFitRange = [ -9999 , -9999 ]
		if( hasattr(userin, 'massRange') ):

			gpFitRange = userin.massRange
			oldBins = np.array([],"d")
			newBins = np.array([],"d")

			for ibin in range(origHisto.GetNbinsX()+1):
				oldBins = np.append( oldBins , origHisto.GetBinLowEdge(ibin+1) )

			for binEdge in oldBins:
				if( binEdge < gpFitRange[0] ): continue
				if( binEdge > gpFitRange[1] ): continue
				newBins = np.append( newBins , binEdge )

			dummyTrimmedHisto = ROOT.TH1D( inHistName+'_trim' , inHistName+'_trim' , len(newBins)-1 , newBins )
			for ibin in range(len(newBins)):
				newBinCenter = dummyTrimmedHisto.GetBinCenter(ibin+1)
				oldBin = origHisto.FindBin( newBinCenter )
				dummyTrimmedHisto.SetBinContent( ibin , origHisto.GetBinContent(oldBin) )
				dummyTrimmedHisto.SetBinError( ibin , origHisto.GetBinError(oldBin) )

			origHisto = dummyTrimmedHisto
			origHisto.SetNameTitle(inHistName,inHistName)


		nRebin = 1
		if( hasattr(userin, 'nRebin') ): nRebin = userin.nRebin
		if( nRebin != 1 ): origHisto.Rebin( nRebin )
		origHisto.Scale( nBkgEvts / origHisto.Integral() )   # This shouldn't do anything, except for when the # events per toy is forced (hacky)

		inShapeName = "Raw Original Template"

		if( userin.useSmoothToys ):
			truthHistName = "basisBkgWS_"+inHistName
			rootBasisFile = ROOT.TFile.Open(userin.outToyBasisFileName)
			truthShapeWS = rootBasisFile.Get(truthHistName)
			truthShapePDF = truthShapeWS.pdf("bkg")
			basisHisto = truthShapeWS.pdf('bkg').createHistogram("basisBkgHisto",truthShapeWS.var("rooMyy"),ROOT.RooFit.Binning(myBins) )
			basisHisto.Scale( nBkgEvts / basisHisto.Integral() )

			# Also get the name of the analytic function used to generate this basis
			allPdfVarNames = []
			allPdfVars = truthShapeWS.allVars()
			variter = allPdfVars.createIterator()
			whichvar = variter.Next()
			while whichvar:
				allPdfVarNames.append(whichvar.GetName())
				whichvar = variter.Next()
			for func in myBkgFunctions:
				isMatchFunc = True
				for varName in allPdfVarNames:
					if( ( varName == "rooMyy" ) or ( varName == "RooM0" ) ): continue
					if( not varName in func[1] ):
						isMatchFunc = False
						#print( 'Variable ',varName,' not found in function ',func[0])
				if( isMatchFunc ):
					print( 'Found matching basis function: ',func[0])
					inShapeName = func[0]
					break

		else: basisHisto = rootFile.Get(origFile)


		# If signal-like bump was injected, need to add this to the basis histogram for plots
		fetchSigShape = False
		if( nSigEvts > 0 ): fetchSigShape = True
		if( fetchSigShape ):
			sigShapeHistoName = 'basisSigHisto__rooMyy'
			sigShapeHisto = inToyTestFile.Get(sigShapeHistoName)
			sigShapeHisto.Scale( nSigEvts / sigShapeHisto.Integral() )
			basisHisto.Add( sigShapeHisto )

		xMin = basisHisto.GetBinLowEdge(1)
		xMax = basisHisto.GetBinLowEdge(nBins+1)

		# Construct the needed histograms to hold the avg shapes

		avgGPhisto = ROOT.TH1D( catName+'_avgGP' , catName+'_avgGP' , nBins , binEdges )
		avgToyhisto = ROOT.TH1D( catName+'_avgToy' , catName+'_avgToy' , nBins , binEdges )

		residGPhisto = ROOT.TH1D( catName+'_resGP' , catName+'_resGP' , nBins , binEdges )
		residToyhisto = ROOT.TH1D( catName+'_resToy' , catName+'_resToy' , nBins , binEdges )

		# debugging
# 		c = ROOT.TCanvas('c','c',600,600)
# 		basisHisto2 = truthShapeWS.pdf('bkg').createHistogram("basisBkgHisto2",truthShapeWS.var("rooMyy"),ROOT.RooFit.Binning(myBins) )
# 		basisHisto2.Scale( nBkgEvts / basisHisto2.Integral() )
# 		basisHisto2.SetLineColor(2)
# 		basisHisto2.Draw('ehist')
# 		basisHisto.SetLineColor(1)
# 		basisHisto.Draw('hist same')
# 		import pdb; pdb.set_trace()


		for ibin in range(nBins):
			avgVal = np.median( genTree['GPR_histVals'].array()[:,ibin] )
			avgValErr = np.std( genTree['GPR_histVals'].array()[:,ibin] )
			avgGPhisto.SetBinContent(ibin+1,avgVal)
			avgGPhisto.SetBinError(ibin+1,avgValErr)

			avgVal = np.median( genTree['Toy_histVals'].array()[:,ibin] )
			avgValErr = np.std( genTree['Toy_histVals'].array()[:,ibin] )
			avgToyhisto.SetBinContent(ibin+1,avgVal)
			avgToyhisto.SetBinError(ibin+1,avgValErr)

		residGPhisto.Add( basisHisto , avgGPhisto , 1 , -1 )
		residGPhisto.Divide( basisHisto )

		residToyhisto.Add( basisHisto , avgToyhisto , 1 , -1 )
		residToyhisto.Divide( basisHisto )

		########## The remainder is all plot styling!!

		### Lost of Details to Make Nice Plots!

		canvas_avgFitShapeName = "canvas_avgFitShape_"+inHistName
		canvas_avgFitShape = ROOT.TCanvas(canvas_avgFitShapeName,canvas_avgFitShapeName,600,600)
		canvas_avgFitShape.cd()
		upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
		lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
		upperPad.Draw()
		upperPad.SetBottomMargin(0.02)
		lowerPad.Draw()
		lowerPad.SetTopMargin(0.02) ;
		lowerPad.SetBottomMargin(0.3) ;

		# The upper pad

		canvas_avgFitShape.cd()
		upperPad.cd()

		yMax = 1.35*basisHisto.GetMaximum()
		leg = ROOT.TLegend(0.65,0.65,0.89,0.89)
		leg.SetBorderSize(0)
		leg.SetFillStyle(-1)

		# If using smooth toys (from analytic functions), plot the original template in the back (otherwise it's a bit messy)
		if( userin.useSmoothToys ):
			origHisto.SetLineColor(1)
			origHisto.SetLineWidth(1)
			origHisto.SetMarkerSize(1)
			origHisto.SetMinimum(0)
			origHisto.SetMaximum(yMax)
			origHisto.GetYaxis().SetTitle("Events / Bin")
			origHisto.GetYaxis().SetTitleSize(0.055)
			origHisto.GetXaxis().SetLabelSize(0.0)
			origHisto.Draw("ephist same")
			leg.AddEntry(origHisto,"Original Template")


		avgToyhisto.SetLineColor(633)
		avgToyhisto.SetLineWidth(2)
		avgToyhisto.SetMarkerColor(633)
		avgToyhisto.SetMarkerSize(0)
		avgToyhisto.SetFillColorAlpha(633,0.3)
		#avgToyhisto.SetFillStyle(3003)
		#avgToyhisto.SetMinimum(0)
		#avgToyhisto.SetMaximum(yMax)
		avgToyhisto.GetXaxis().SetTitleSize(0.0)
		avgToyhisto.GetXaxis().SetLabelSize(0.0)
		avgToyhisto.GetYaxis().SetTitle("Events / Bin")
		avgToyhisto.GetYaxis().SetTitleSize(0.055)
		avgToyhisto.Draw("e4 same")
		leg.AddEntry(avgToyhisto,"Avg. Toy Shape")


		avgGPhisto.SetLineColor(601)
		avgGPhisto.SetLineWidth(2)
		avgGPhisto.SetMarkerColor(601)
		avgGPhisto.SetMarkerSize(0)
		avgGPhisto.SetFillColorAlpha(601,0.3)
		#avgGPhisto.SetFillStyle(3003)
		#avgGPhisto.SetMinimum(0)
		#avgGPhisto.SetMaximum(yMax)
		avgGPhisto.GetXaxis().SetTitleSize(0.0)
		avgGPhisto.GetXaxis().SetLabelSize(0.0)
		avgGPhisto.GetYaxis().SetTitle("Events / Bin")
		avgGPhisto.GetYaxis().SetTitleSize(0.055)
		avgGPhisto.Draw("e4 same")
		leg.AddEntry(avgGPhisto,"Avg. GP Fit")


		bkgTemplateCentralToy = avgToyhisto.Clone("bkgTemplateCentralToy")
		bkgTemplateCentralToy.SetLineColor(633)
		bkgTemplateCentralToy.SetLineWidth(2)
		bkgTemplateCentralToy.SetMarkerColor(633)
		bkgTemplateCentralToy.SetMarkerSize(0)
		bkgTemplateCentralToy.SetFillStyle(0)
		bkgTemplateCentralToy.Draw("histsame")

		bkgTemplateCentralGP = avgGPhisto.Clone("bkgTemplateCentralGP")
		bkgTemplateCentralGP.SetLineColor(601)
		bkgTemplateCentralGP.SetLineWidth(2)
		bkgTemplateCentralGP.SetMarkerColor(601)
		bkgTemplateCentralGP.SetMarkerSize(0)
		bkgTemplateCentralGP.SetFillStyle(0)
		bkgTemplateCentralGP.Draw("histsame")


		basisHisto.SetLineColor(6)
		basisHisto.SetLineWidth(1)
		basisHisto.SetMarkerSize(0)
		basisHisto.SetMinimum(0)
		basisHisto.SetMaximum(yMax)
		basisHisto.GetYaxis().SetTitle("Events / Bin")
		basisHisto.GetYaxis().SetTitleSize(0.055)
		basisHisto.GetXaxis().SetLabelSize(0.0)
		basisHisto.Draw("hist same")
		if( not userin.useSmoothToys ): leg.AddEntry(basisHisto,"Original Template")
		else: leg.AddEntry(basisHisto,"Smooth Truth Shape")


		leg.Draw()

		ROOT.ATLAS_LABEL(0.23,0.85)
		ROOT.myText(0.34,0.85,1,"Internal")
		ROOT.myText(0.23,0.80,1,"Toy Test - Average Fit Shape",0.04)
		ROOT.myText(0.23,0.76,1,catName,0.04)

		canvas_avgFitShape.Update()


		# The lower pad
		#    NOTE: If data sidebands are provided, will plot the difference of the original and
		#    smoothed templates in reference to the data sidebands. Otherwise, will plot only the
		#    difference of the GPR smoothed template to the original.

		canvas_avgFitShape.cd()
		lowerPad.cd()

		lowerYmin = -9999
		lowerYmax = -9999
		lowerYmax = residToyhisto.GetMaximum()
		if( residGPhisto.GetMaximum() > lowerYmax ): lowerYmax = residGPhisto.GetMaximum()
		lowerYmin = residToyhisto.GetMinimum()
		if( residGPhisto.GetMinimum() < lowerYmin ): lowerYmin = residGPhisto.GetMinimum()
		lowerYmax += 0.1*lowerYmax
		lowerYmin -= 0.1*lowerYmax

		refLine = ROOT.TH1I("refLine","refLine",1,xMin,xMax)
		refLine.SetBinContent(1,0)
		refLine.SetLineColor(1)
		refLine.SetLineWidth(1)
		refLine.SetLineStyle(7)
		refLine.SetMinimum(lowerYmin)
		refLine.SetMaximum(lowerYmax)
		refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
		refLine.GetXaxis().SetTitleSize(0.13)
		refLine.GetXaxis().SetTitleOffset(1.0)
		refLine.GetXaxis().SetLabelSize(0.09)
		refLine.GetYaxis().SetTitle("Residual [%]")
		refLine.GetYaxis().SetTitleSize(0.11)
		refLine.GetYaxis().SetTitleOffset(0.55)
		refLine.GetYaxis().SetLabelSize(0.09)
		refLine.Draw()

		residToyhisto.SetLineColor(633)
		residToyhisto.SetMarkerSize(0)
		residToyhisto.SetMinimum(lowerYmin)
		residToyhisto.SetMaximum(lowerYmax)
		residToyhisto.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
		residToyhisto.GetXaxis().SetTitleSize(0.13)
		residToyhisto.GetXaxis().SetTitleOffset(1.0)
		residToyhisto.GetXaxis().SetLabelSize(0.09)
		residToyhisto.GetYaxis().SetTitle("Residual [%]")
		residToyhisto.GetYaxis().SetTitleSize(0.11)
		residToyhisto.GetYaxis().SetTitleOffset(0.55)
		residToyhisto.GetYaxis().SetLabelSize(0.09)
		residToyhisto.Draw("hist same")

		residGPhisto.SetLineColor(601)
		residGPhisto.SetMarkerSize(0)
		residGPhisto.SetMinimum(lowerYmin)
		residGPhisto.SetMaximum(lowerYmax)
		residGPhisto.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
		residGPhisto.GetXaxis().SetTitleSize(0.13)
		residGPhisto.GetXaxis().SetTitleOffset(1.0)
		residGPhisto.GetXaxis().SetLabelSize(0.09)
		residGPhisto.GetYaxis().SetTitle("Residual [%]")
		residGPhisto.GetYaxis().SetTitleSize(0.11)
		residGPhisto.GetYaxis().SetTitleOffset(0.55)
		residGPhisto.GetYaxis().SetLabelSize(0.09)
		residGPhisto.Draw("hist same")

		refLine.Draw("same")

		canvas_avgFitShape.Update()

		canvas_avgFitShape.Print("ToyTest_AvgFitShape_"+catName+".pdf")

		### Now, make a plot of the avg. fitted spurious signal for each analytic function ###

		canvas_spurSigDist = ROOT.TCanvas( "canvas_spurSigDist" , "canvas_spurSigDist" , 1200 , 800 )
		spursigGPhistos = []
		spursigToyhistos = []

		gprHistoGaussFits = []
		toyHistoGaussFits = []

		gprSpurSig = []
		toySpurSig = []

		gprDeltaS = []
		toyDeltaS = []

		spurSigTest_legs = []

		nSigInject = genTree['nSigEvts'].array()[0]

		nFuncs = len( myBkgFunctions )
		if( nFuncs <= 3 ): canvas_spurSigDist.Divide( nFuncs , 1 )
		elif( nFuncs <= 8 ): canvas_spurSigDist.Divide( int(np.ceil(nFuncs/2)) , 2 )
		else: canvas_spurSigDist.Divide( int(np.ceil(nFuncs/3)) , 3 )


		for ifunc,funcDef in enumerate(myBkgFunctions):

			canvas_spurSigDist.GetPad( ifunc+1 ).cd()

			testFunction = funcDef[0]

			treeName = inHistName+"_ToyTests_func_"+testFunction
			funcTree = inFile[treeName]

			gprSpurSig.append( funcTree['nSpurSigValue_GPR'].array() - nSigInject )
			toySpurSig.append( funcTree['nSpurSigValue_Toy'].array() - nSigInject )

			if( 'GPR_deltaS' in np.array(funcTree.keys(),'str') ):
				gprDeltaS.append( funcTree['GPR_deltaS'].array() )
				toyDeltaS.append( funcTree['Toy_deltaS'].array() )
			else:
				print("WARNING: These seem to be old Toy Test output files; they do not contain deltaS.")
				print("   Analysis with these values will be skipped.")
				gprDeltaS.append( np.zeros( len(gprSpurSig[-1]),'d' ) )
				toyDeltaS.append( np.zeros( len(toySpurSig[-1]),'d' ) )

			minSig = np.min( toySpurSig[ifunc] )
			if( np.min( gprSpurSig[ifunc] ) < minSig ): minSig = np.min( gprSpurSig[ifunc] )
			xMinsig = minSig - 0.05*(np.abs(minSig))

			maxSig = np.max( toySpurSig[ifunc] )
			if( np.max( gprSpurSig[ifunc] ) > maxSig ): maxSig = np.max( gprSpurSig[ifunc] )
			xMaxsig = maxSig + 0.05*(np.abs(maxSig))

			spursigGPhistos.append( ROOT.TH1D( catName+'_GPspursig' , catName+'_GPspursig' , 50 , xMinsig , xMaxsig ) )
			spursigToyhistos.append( ROOT.TH1D( catName+'_toySpurSig[ifunc]' , catName+'_toySpurSig[ifunc]' , 50 , xMinsig , xMaxsig ) )

			spursigGPhistos[ifunc].FillN( len(gprSpurSig[ifunc]) , gprSpurSig[ifunc] , np.ones(len(gprSpurSig[ifunc])) , 1 )
			spursigToyhistos[ifunc].FillN( len(toySpurSig[ifunc]) , toySpurSig[ifunc] , np.ones(len(toySpurSig[ifunc])) , 1 )

			spurSigTest_legs.append( ROOT.TLegend(0.65,0.65,0.89,0.89) )
			spurSigTest_legs[ifunc].SetBorderSize(0)
			spurSigTest_legs[ifunc].SetFillStyle(-1)

			spursigToyhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
			if( nSigInject == 0 ): spursigToyhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal" )
			spursigToyhistos[ifunc].GetYaxis().SetTitle( "Number of Entries" )
			spursigToyhistos[ifunc].SetLineColor(2)
			spursigToyhistos[ifunc].Draw("hist")
			spurSigTest_legs[ifunc].AddEntry( spursigToyhistos[ifunc] , "Raw Toy" , "l" )

			spursigGPhistos[ifunc].SetLineColor(4)
			spursigGPhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
			if( nSigInject == 0 ): spursigGPhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal" )
			spursigGPhistos[ifunc].GetYaxis().SetTitle( "Number of Entries" )
			spursigGPhistos[ifunc].Draw("hist same")
			spurSigTest_legs[ifunc].AddEntry( spursigGPhistos[ifunc] , "GP Toy" , "l" )

			### Fit a Gaussian to the spur sig distribution

			gprFitName = "gaussGPR_fit_"+str(testFunction)
			toyFitName = "gaussToy_fit_"+str(testFunction)

			gprHistoGaussFits.append( ROOT.TF1(gprFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )
			toyHistoGaussFits.append( ROOT.TF1(toyFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )

			gprHistoGaussFits[ifunc].SetParLimits(0,xMinsig,xMaxsig)
			gprHistoGaussFits[ifunc].SetParLimits(0,xMinsig,xMaxsig)
			gprHistoGaussFits[ifunc].SetParameters(spursigGPhistos[ifunc].GetMean(),spursigGPhistos[ifunc].GetRMS());

			toyHistoGaussFits[ifunc].SetParLimits(1,0,xMaxsig-xMinsig)
			toyHistoGaussFits[ifunc].SetParLimits(1,0,xMaxsig-xMinsig)
			toyHistoGaussFits[ifunc].SetParameters(spursigToyhistos[ifunc].GetMean(),spursigToyhistos[ifunc].GetRMS());

			gprHistoGaussFits[ifunc].SetLineColor(601)
			toyHistoGaussFits[ifunc].SetLineColor(633)
			gprHistoGaussFits[ifunc].SetLineStyle(9)
			toyHistoGaussFits[ifunc].SetLineStyle(9)
			gprHistoGaussFits[ifunc].SetLineWidth(2)
			toyHistoGaussFits[ifunc].SetLineWidth(2)
			spursigGPhistos[ifunc].Draw()
			spursigToyhistos[ifunc].Draw("same")
			spursigGPhistos[ifunc].Fit(gprFitName,"emr same")
			spursigToyhistos[ifunc].Fit(toyFitName,"emr same")
			spursigGPhistos[ifunc].DrawCopy("same")
			spursigToyhistos[ifunc].DrawCopy("same")

			spurSigTest_legs[ifunc].Draw()

			ROOT.ATLAS_LABEL(0.23,0.85)
			ROOT.myText(0.4,0.85,1,"Internal")
			ROOT.myText(0.23,0.80,1,"Toy Test - Spurious Signal",0.04)
			ROOT.myText(0.23,0.76,1,catName,0.04)
			ROOT.myText(0.23,0.72,1,testFunction,0.04)

			# Include text of the mean/sigma of the spurious signal distributions on the plots
			gpTxt = ("GP: %2.3f  #pm %2.3f" % (np.mean(gprSpurSig[ifunc]),np.std(gprSpurSig[ifunc])) )
			toyTxt = ("Raw: %2.3f  #pm %2.3f" % (np.mean(toySpurSig[ifunc]),np.std(toySpurSig[ifunc])) )
			ROOT.myText(0.23,0.6,1,gpTxt,0.04)
			ROOT.myText(0.23,0.56,1,toyTxt,0.04)

			canvas_spurSigDist.Update()

		canvas_spurSigDist.Print("ToyTest_FitSigVals_"+catName+".pdf")



		### Now, make a plot of all the toy shapes, and the fitted GP shapes ###

		canvas_allGPfits = ROOT.TCanvas( "canvas_allGPfits" , "canvas_allGPfits" , 800 , 800 )

		allGPfits = []
		allToys = []

		yMin = np.min(genTree['Toy_histVals'].array())
		if( np.min(genTree['GPR_histVals'].array()) < yMin ): yMin = np.min(genTree['GPR_histVals'].array())
		yMax = np.max(genTree['Toy_histVals'].array())
		if( np.max(genTree['GPR_histVals'].array()) > yMax ): yMax = np.max(genTree['GPR_histVals'].array())
		yMin = yMin - 0.1*yMin
		yMax = yMax + 0.1*yMax


		for itoy in range(nToys):

			dummyName = "GPR_shape_toy"+str(itoy)
			allGPfits.append( ROOT.TH1D( dummyName , dummyName , nBins , binEdges ) )
			makeHistoFromArray( genTree['GPR_histVals'].array()[itoy,:] , allGPfits[itoy] )

			dummyName = "Toy_shape_toy"+str(itoy)
			allToys.append( ROOT.TH1D( dummyName , dummyName , nBins , binEdges ) )
			makeHistoFromArray( genTree['Toy_histVals'].array()[itoy,:] , allToys[itoy] )

			allToys[-1].SetMinimum(yMin)
			allToys[-1].SetMaximum(yMax)
			allToys[-1].GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
			allToys[-1].GetYaxis().SetTitle("Events / Bin (S+B Fits)")

			allGPfits[-1].SetLineColorAlpha(601,0.1)
			allToys[-1].SetLineColorAlpha(633,0.1)
# 				allGPfits[-1].SetLineStyle(9)
# 				allToys[-1].SetLineStyle(9)
# 				allGPfits[-1].SetLineWidth(2)
# 				allToys[-1].SetLineWidth(2)

			allToys[-1].Draw("hist same")
			allGPfits[-1].Draw("hist same")

			if( itoy==0 ):
				allToyAndGPs_leg = ROOT.TLegend(0.6,0.6,0.89,0.89)
				allToyAndGPs_leg.SetFillColor(0)
				allToyAndGPs_leg.SetBorderSize(0)
				allToyAndGPs_leg.AddEntry( allGPfits[-1] , "SB Fits to GP Toy" , "l" )
				allToyAndGPs_leg.AddEntry( allToys[-1] , "SB Fits to Raw Toy" , "l" )
				allToyAndGPs_leg.AddEntry( basisHisto , "Toy Basis" )
				allToyAndGPs_leg.Draw()

		basisHisto.Draw("hist same")

		ROOT.ATLAS_LABEL(0.23,0.85)
		ROOT.myText(0.4,0.85,1,"Internal")
		ROOT.myText(0.23,0.80,1,"Toy Test - All Toys and GP Fits",0.04)
		ROOT.myText(0.23,0.76,1,catName,0.04)
		ROOT.myText(0.23,0.72,1,testFunction,0.04)

		canvas_allGPfits.Update()
		canvas_allGPfits.Print("ToyTest_allToysAndGPfits_"+catName+".pdf")



		### Now, make a plot of the avg. fit shape (from the S+B fits) for each analytic function ###

		canvas_SBfits = ROOT.TCanvas( "canvas_SBfits" , "canvas_SBfits" , 1200 , 800 )
		spursigSBFit_legs = []

		nFuncs = len( myBkgFunctions )
		if( nFuncs <= 3 ): canvas_SBfits.Divide( nFuncs , 1 )
		elif( nFuncs <= 8 ): canvas_SBfits.Divide( int(np.ceil(nFuncs/2)) , 2 )
		else: canvas_SBfits.Divide( int(np.ceil(nFuncs/3)) , 3 )

		sbFitHistosGP = []
		sbFitHistosToy = []

		for ifunc,funcDef in enumerate(myBkgFunctions):

			canvas_SBfits.GetPad( ifunc+1 ).cd()

			testFunction = funcDef[0]
			treeName = inHistName+"_ToyTests_func_"+testFunction
			funcTree = inFile[treeName]

			gprSBfits = funcTree['GPR_sbFitVals'].array()
			toySBfits = funcTree['Toy_sbFitVals'].array()

			yMin = np.min(toySBfits)
			if( np.min(gprSBfits) < yMin ): yMin = np.min(gprSBfits)
			yMax = np.max(toySBfits)
			if( np.max(gprSBfits) > yMax ): yMax = np.max(gprSBfits)
			yMin = yMin - 0.1*yMin
			yMax = yMax + 0.1*yMax

			sbFitHistosGP_holder = []
			sbFitHistosToy_holder = []

			for itoy in range(nToys):

				dummyName = "GPR_SBfit_func_"+testFunction+"+_toy"+str(itoy)
				sbFitHistosGP_holder.append( ROOT.TH1D( dummyName , dummyName , nBins , binEdges ) )
				makeHistoFromArray( gprSBfits[itoy] , sbFitHistosGP_holder[-1] )

				dummyName = "Toy_SBfit_func_"+testFunction+"+_toy"+str(itoy)
				sbFitHistosToy_holder.append( ROOT.TH1D( dummyName , dummyName , nBins , binEdges ) )
				makeHistoFromArray( toySBfits[itoy] , sbFitHistosToy_holder[-1] )

				sbFitHistosToy_holder[-1].SetMinimum(yMin)
				sbFitHistosToy_holder[-1].SetMaximum(yMax)
				sbFitHistosToy_holder[-1].GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
				sbFitHistosToy_holder[-1].GetYaxis().SetTitle("Events / Bin (S+B Fits)")

				sbFitHistosGP_holder[-1].SetLineColorAlpha(601,0.1)
				sbFitHistosToy_holder[-1].SetLineColorAlpha(633,0.1)
# 				sbFitHistosGP_holder[-1].SetLineStyle(9)
# 				sbFitHistosToy_holder[-1].SetLineStyle(9)
# 				sbFitHistosGP_holder[-1].SetLineWidth(2)
# 				sbFitHistosToy_holder[-1].SetLineWidth(2)

				sbFitHistosToy_holder[-1].Draw("hist same")
				sbFitHistosGP_holder[-1].Draw("hist same")

				if( itoy==0 ):
					spursigSBFit_legs.append( ROOT.TLegend(0.6,0.6,0.89,0.89) )
					spursigSBFit_legs[-1].SetFillColor(0)
					spursigSBFit_legs[-1].SetBorderSize(0)
					spursigSBFit_legs[-1].AddEntry( sbFitHistosGP_holder[-1] , "SB Fits to GP Toy" , "l" )
					spursigSBFit_legs[-1].AddEntry( sbFitHistosToy_holder[-1] , "SB Fits to Raw Toy" , "l" )
					spursigSBFit_legs[-1].AddEntry( basisHisto , "Toy Basis" )
					spursigSBFit_legs[-1].Draw()

			basisHisto.Draw("hist same")

			ROOT.ATLAS_LABEL(0.23,0.85)
			ROOT.myText(0.4,0.85,1,"Internal")
			ROOT.myText(0.23,0.80,1,"Toy Test - All S+B Fits",0.04)
			ROOT.myText(0.23,0.76,1,catName,0.04)
			ROOT.myText(0.23,0.72,1,testFunction,0.04)

			sbFitHistosGP.append( sbFitHistosGP_holder )
			sbFitHistosToy.append( sbFitHistosToy_holder )


		canvas_SBfits.Update()
		canvas_SBfits.Print("ToyTest_SBFitVals_"+catName+".pdf")


		### Make a plot of the spurious signal versus x (if doing a mass scan) ###

		if( hasattr(userin, 'toyMassScanPoints') ):

			nMassPoints = len( userin.toyMassScanPoints )

			if( nMassPoints > 1 ):

				canvas_massScan = ROOT.TCanvas( "canvas_massScan" , "canvas_massScan" , 1200 , 800 )

				massScanGraphs_GP = []
				massScanGraphs_Toy = []
				massScanMultiGraphs = []
				massScanLegs = []

				nFuncs = len( myBkgFunctions )
				if( nFuncs <= 3 ): canvas_massScan.Divide( nFuncs , 1 )
				elif( nFuncs <= 8 ): canvas_massScan.Divide( int(np.ceil(nFuncs/2)) , 2 )
				else: canvas_massScan.Divide( int(np.ceil(nFuncs/3)) , 3 )
				nMasses = len(userin.toyMassScanPoints)

				massScanVals_GP = np.zeros( (nFuncs,nMasses) )
				massScanErrs_GP = np.zeros( (nFuncs,nMasses) )

				massScanVals_Toy = np.zeros( (nFuncs,nMasses) )
				massScanErrs_Toy = np.zeros( (nFuncs,nMasses) )

				for ifunc,funcDef in enumerate(myBkgFunctions):

					testFunction = funcDef[0]
					treeName = inHistName+"_ToyTests_func_"+testFunction
					funcTree = inFile[treeName]

					massScanLegs.append( ROOT.TLegend(0.65,0.65,0.89,0.89) )
					massScanLegs[ifunc].SetBorderSize(0)
					massScanLegs[ifunc].SetFillStyle(-1)

					for imass,mass in enumerate(userin.toyMassScanPoints):

						hold_gpr = funcTree['nSpurSigValue_GPR'].array()[ funcTree['sigMass'].array()==mass ]
						hold_toy = funcTree['nSpurSigValue_Toy'].array()[ funcTree['sigMass'].array()==mass ]

						massScanVals_GP[ifunc][imass] = np.mean(hold_gpr)
						massScanErrs_GP[ifunc][imass] = np.std(hold_gpr)

						massScanVals_Toy[ifunc][imass] = np.mean(hold_toy)
						massScanErrs_Toy[ifunc][imass] = np.std(hold_toy)

					canvas_massScan.GetPad(ifunc+1).cd()


					massScanGraphs_GP.append( ROOT.TGraphErrors() )
					massScanGraphs_Toy.append( ROOT.TGraphErrors() )
					for imass in range(nMasses):

						massScanGraphs_GP[ifunc].SetPoint( imass , np.array(userin.toyMassScanPoints)[imass] , massScanVals_GP[ifunc][imass] )
						massScanGraphs_GP[ifunc].SetPointError( imass , 0.0 , massScanErrs_GP[ifunc][imass] )

						massScanGraphs_Toy[ifunc].SetPoint( imass , np.array(userin.toyMassScanPoints)[imass] , massScanVals_Toy[ifunc][imass] )
						massScanGraphs_Toy[ifunc].SetPointError( imass , 0.0 , massScanErrs_Toy[ifunc][imass] )


					massScanGraphs_GP[ifunc].SetLineWidth(2)
					massScanGraphs_GP[ifunc].SetLineColor(4)
					massScanGraphs_GP[ifunc].SetMarkerColor(4)
					#massScanGraphs_GP[ifunc].SetFillStyle(3004)
					massScanGraphs_GP[ifunc].SetFillStyle(3001)
					massScanGraphs_GP[ifunc].SetFillColorAlpha(4,0.8)

					massScanGraphs_Toy[ifunc].SetLineWidth(2)
					massScanGraphs_Toy[ifunc].SetLineColor(2)
					massScanGraphs_Toy[ifunc].SetMarkerColor(2)
					#massScanGraphs_Toy[ifunc].SetFillStyle(3005)
					massScanGraphs_Toy[ifunc].SetFillStyle(3003)
					massScanGraphs_Toy[ifunc].SetFillColorAlpha(2,0.8)

					massScanLegs[ifunc].AddEntry( massScanGraphs_GP[ifunc] , "GP Toy" , "l")
					massScanLegs[ifunc].AddEntry( massScanGraphs_Toy[ifunc] , "Raw Toy" , "l")

					massScanMultiGraphs.append( ROOT.TMultiGraph() )
					massScanMultiGraphs[ifunc].Add( massScanGraphs_Toy[ifunc] )
					massScanMultiGraphs[ifunc].Add( massScanGraphs_GP[ifunc] )

					massScanMultiGraphs[ifunc].GetXaxis().SetTitle( "M_{#gamma#gamma} [GeV]" )
					massScanMultiGraphs[ifunc].GetYaxis().SetTitle( "Fitted Spurious Signal" )
					massScanMultiGraphs[ifunc].Draw("ALP3")
					massScanLegs[ifunc].Draw()

					ROOT.ATLAS_LABEL(0.23,0.85)
					ROOT.myText(0.4,0.85,1,"Internal")
					ROOT.myText(0.23,0.80,1,"Toy Test - Spurious Signal",0.04)
					ROOT.myText(0.23,0.76,1,catName,0.04)
					ROOT.myText(0.23,0.72,1,testFunction,0.04)


				canvas_massScan.Update()
				canvas_massScan.Print("ToyTest_SpurSigMassScan_"+catName+".pdf")
				canvas_massScan.Close()




				### Make plots of the spurious signal histograms, but for each mass point (this will be a lot of plots)

				canvases_massScan_spurSig = []
				spurSigTest_massScan_legs = []

				spursigGPhistos_massScan = []
				spursigToyhistos_massScan = []

				gprSpurSig_massScan = []
				toySpurSig_massScan = []


				gprDeltaS_massScan = []
				toyDeltaS_massScan = []


				nCanvasesPerFunction = int( np.ceil( nMassPoints / 12. ) )


				for ifunc,funcDef in enumerate(myBkgFunctions):

					testFunction = funcDef[0]

					canvases_massScan_spurSig_funcHold = []
					for iCan in range(nCanvasesPerFunction):
						canName = "canvas_spurSigMeas_massScan_group"+str(iCan)+"_func_"+testFunction
						canvases_massScan_spurSig_funcHold.append( ROOT.TCanvas(canName,canName,1200,800) )
						canvases_massScan_spurSig_funcHold[-1].Divide( 4,3 )
					canvases_massScan_spurSig.append( canvases_massScan_spurSig_funcHold )


					treeName = inHistName+"_ToyTests_func_"+testFunction
					funcTree = inFile[treeName]


					spursigGPhistos_massScan_func = []
					spursigToyhistos_massScan_func = []

					gprSpurSig_massScanHolder = []
					toySpurSig_massScanHolder = []

					gprDeltaS_massScanHolder = []
					toyDeltaS_massScanHolder = []

					spurSigTest_massScan_legs.append( ROOT.TLegend(0.65,0.65,0.89,0.89) )
					spurSigTest_massScan_legs[ifunc].SetBorderSize(0)
					spurSigTest_massScan_legs[ifunc].SetFillStyle(-1)


					for imass,mass in enumerate(userin.toyMassScanPoints):

						whichCanvas = int( np.floor( imass / 12 ) )
						whichPad = int( imass - whichCanvas*12 + 1 )

						canvases_massScan_spurSig[ifunc][whichCanvas].cd()
						canvases_massScan_spurSig[ifunc][whichCanvas].GetPad(whichPad).cd()

						gprSpurSig_massScanHolder.append( funcTree['nSpurSigValue_GPR'].array()[ funcTree['sigMass'].array()==mass ] - nSigInject )
						toySpurSig_massScanHolder.append( funcTree['nSpurSigValue_Toy'].array()[ funcTree['sigMass'].array()==mass ] - nSigInject )

						if( 'GPR_deltaS' in np.array(funcTree.keys(),'str') ):
							gprDeltaS_massScanHolder.append( funcTree['GPR_deltaS'].array()[ funcTree['sigMass'].array()==mass ] )
							toyDeltaS_massScanHolder.append( funcTree['Toy_deltaS'].array()[ funcTree['sigMass'].array()==mass ] )
						else:
							#print("WARNING: These seem to be old Toy Test output files; they do not contain deltaS.")
							#print("   Analysis with these values will be skipped.")
							gprDeltaS_massScanHolder.append( np.zeros( len(gprSpurSig_massScanHolder[-1]),'d' ) )
							toyDeltaS_massScanHolder.append( np.zeros( len(toySpurSig_massScanHolder[-1]),'d' ) )

						minSig = np.min( toySpurSig_massScanHolder[-1] )
						if( np.min( gprSpurSig_massScanHolder[-1] ) < minSig ): minSig = np.min( gprSpurSig_massScanHolder[-1] )
						xMinsig = minSig - 0.05*(np.abs(minSig))

						maxSig = np.max( toySpurSig_massScanHolder[-1] )
						if( np.max( gprSpurSig_massScanHolder[-1] ) > maxSig ): maxSig = np.max( gprSpurSig_massScanHolder[-1] )
						xMaxsig = maxSig + 0.05*(np.abs(maxSig))

						histNameStub = catName+'_func_'+testFunction+'_m'+str(mass)
						spursigGPhistos_massScan_func.append( ROOT.TH1D( histNameStub+'_GPspursig' , histNameStub+'_GPspursig' , 50 , xMinsig , xMaxsig ) )
						spursigToyhistos_massScan_func.append( ROOT.TH1D( histNameStub+'_toySpurSig_massScanHolder[-1]' , histNameStub+'_toySpurSig_massScanHolder[-1]' , 50 , xMinsig , xMaxsig ) )

						spursigGPhistos_massScan_func[-1].FillN( len(gprSpurSig_massScanHolder[-1]) , gprSpurSig_massScanHolder[-1] , np.ones(len(gprSpurSig_massScanHolder[-1])) , 1 )
						spursigToyhistos_massScan_func[-1].FillN( len(toySpurSig_massScanHolder[-1]) , toySpurSig_massScanHolder[-1] , np.ones(len(toySpurSig_massScanHolder[-1])) , 1 )

						spursigToyhistos_massScan_func[-1].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
						if( nSigInject == 0 ): spursigToyhistos_massScan_func[-1].GetXaxis().SetTitle( "Fitted Spurious Signal" )
						spursigToyhistos_massScan_func[-1].GetYaxis().SetTitle( "Number of Entries" )
						spursigToyhistos_massScan_func[-1].SetLineColor(2)
						spursigToyhistos_massScan_func[-1].Draw("hist")
						if( whichCanvas == 0 and whichPad == 1 ):
							spurSigTest_massScan_legs[ifunc].AddEntry( spursigToyhistos_massScan_func[-1] , "Raw Toy" , "l" )

						spursigGPhistos_massScan_func[-1].SetLineColor(4)
						spursigGPhistos_massScan_func[-1].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
						if( nSigInject == 0 ): spursigGPhistos_massScan_func[-1].GetXaxis().SetTitle( "Fitted Spurious Signal" )
						spursigGPhistos_massScan_func[-1].GetYaxis().SetTitle( "Number of Entries" )
						spursigGPhistos_massScan_func[-1].Draw("hist same")
						if( whichCanvas == 0 and whichPad == 1 ):
							spurSigTest_massScan_legs[ifunc].AddEntry( spursigGPhistos_massScan_func[-1] , "GP Toy" , "l" )

						### Fit a Gaussian to the spur sig distribution

						gprFitName = "gaussGPR_fit_"+str(testFunction)
						toyFitName = "gaussToy_fit_"+str(testFunction)

						gprHistoGaussFits.append( ROOT.TF1(gprFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )
						toyHistoGaussFits.append( ROOT.TF1(toyFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )

						gprHistoGaussFits[-1].SetParLimits(0,xMinsig,xMaxsig)
						gprHistoGaussFits[-1].SetParLimits(0,xMinsig,xMaxsig)
						gprHistoGaussFits[-1].SetParameters(spursigGPhistos_massScan_func[-1].GetMean(),spursigGPhistos_massScan_func[-1].GetRMS());

						toyHistoGaussFits[-1].SetParLimits(1,0,xMaxsig-xMinsig)
						toyHistoGaussFits[-1].SetParLimits(1,0,xMaxsig-xMinsig)
						toyHistoGaussFits[-1].SetParameters(spursigToyhistos_massScan_func[-1].GetMean(),spursigToyhistos_massScan_func[-1].GetRMS());

						gprHistoGaussFits[-1].SetLineColor(601)
						toyHistoGaussFits[-1].SetLineColor(633)
						gprHistoGaussFits[-1].SetLineStyle(9)
						toyHistoGaussFits[-1].SetLineStyle(9)
						gprHistoGaussFits[-1].SetLineWidth(2)
						toyHistoGaussFits[-1].SetLineWidth(2)
						spursigGPhistos_massScan_func[-1].Draw()
						spursigToyhistos_massScan_func[-1].Draw("same")
						spursigGPhistos_massScan_func[-1].Fit(gprFitName,"emr same")
						spursigToyhistos_massScan_func[-1].Fit(toyFitName,"emr same")
						spursigGPhistos_massScan_func[-1].DrawCopy("same")
						spursigToyhistos_massScan_func[-1].DrawCopy("same")


						spurSigTest_massScan_legs[ifunc].Draw()

						ROOT.ATLAS_LABEL(0.23,0.85)
						ROOT.myText(0.4,0.85,1,"Internal")
						ROOT.myText(0.23,0.80,1,"Toy Test - Spurious Signal",0.04)
						ROOT.myText(0.23,0.76,1,catName,0.04)
						ROOT.myText(0.23,0.72,1,testFunction,0.04)

						# Include text of the mean/sigma of the spurious signal distributions on the plots
						gpTxt = ("GP: %2.3f  #pm %2.3f" % (np.mean(gprSpurSig_massScanHolder[-1]),np.std(gprSpurSig_massScanHolder[-1])) )
						toyTxt = ("Raw: %2.3f  #pm %2.3f" % (np.mean(toySpurSig_massScanHolder[-1]),np.std(toySpurSig_massScanHolder[-1])) )
						ROOT.myText(0.23,0.6,1,gpTxt,0.04)
						ROOT.myText(0.23,0.56,1,toyTxt,0.04)

						canvases_massScan_spurSig[ifunc][whichCanvas].Update()

					gprSpurSig_massScan.append( gprSpurSig_massScanHolder )
					toySpurSig_massScan.append( toySpurSig_massScanHolder )

					spursigGPhistos_massScan.append( spursigGPhistos_massScan_func )
					spursigToyhistos_massScan.append( spursigToyhistos_massScan_func )

					gprDeltaS_massScan.append( gprDeltaS_massScanHolder )
					toyDeltaS_massScan.append( toyDeltaS_massScanHolder )

					for iCan in range(nCanvasesPerFunction):
						canvases_massScan_spurSig[ifunc][iCan].Update()
						canvases_massScan_spurSig[ifunc][iCan].Print("ToyTest_SpurSigVals_func_"+testFunction+"_group"+str(iCan)+"_"+catName+".pdf")


		### Lastly, make a histogram of the fitted length scales from the GP fits

		canvas_GPLengthScale = ROOT.TCanvas( "canvas_GPLengthScale" , "canvas_GPLengthScale" , 800 , 600 )
		canvas_GPLengthScale.cd()

		fittedLengthScales = genTree['GP_length_scale'].array()

		lengthScaleHisto = ROOT.TH1D( "lengthScaleHisto" , "lengthScaleHisto" , 200 , np.min(fittedLengthScales)-1e-5 , np.max(fittedLengthScales)+1e-5 )
		lengthScaleHisto.FillN( len(fittedLengthScales) , fittedLengthScales , np.ones(len(fittedLengthScales)) )

		lengthScaleHisto.SetStats(0)
		lengthScaleHisto.SetLineColor(1)
		lengthScaleHisto.SetLineWidth(2)
		lengthScaleHisto.GetXaxis().SetTitle("GP Length Scale")
		lengthScaleHisto.GetYaxis().SetTitle("Entries / Bin")

		ROOT.ATLAS_LABEL(0.23,0.85)
		ROOT.myText(0.34,0.85,1,"Internal")
		ROOT.myText(0.23,0.80,1,"Toy Test - Fitted Length Scale",0.04)
		ROOT.myText(0.23,0.76,1,catName,0.04)

		lengthScaleHisto.Draw("hist")

		canvas_GPLengthScale.Print("ToyTest_FitLengthScale_"+catName+".pdf")


		# Also print some useful information for the user

		toyCSVfileName = "toyTestResults_"+inHistName+".csv"
		with open(toyCSVfileName, mode='w') as outputCSVfile:
			csv_writer = csv.writer(outputCSVfile) #, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

			print(" ---------------------- ")
			print(" ---------------------- ")
			print(" Toy Test - Information ")
			print(" ---------------------- ")

			if( inShapeName != "Raw Original Template" ):
				print("--- Toy Basis Function = ", inShapeName," ---")
				csv_writer.writerow(['Toy Basis Function = ',inShapeName])
				csv_params_row_labels = []
				csv_params_row_values = []
				if( allPdfVarNames ):
					for varName in allPdfVarNames:
						if( ( varName == "rooMyy" ) or ( varName == "RooM0" ) ): continue
						csv_params_row_labels.append( varName )
						csv_params_row_values.append( truthShapeWS.var(varName).getValV() )
						print("   Variable '",varName,"' = ",truthShapeWS.var(varName).getValV()," +/-",truthShapeWS.var(varName).getError())
					csv_writer.writerow(csv_params_row_labels)
					csv_writer.writerow(csv_params_row_values)
			else:
				print(" Using Raw Input Template with Poisson Bin Fluctuations to Generate Toys. ")
				csv_writer.writerow(['Toy Basis Function = ','Poisson Varied Input Template'])

			csv_writer.writerow([])

			csv_writer.writerow( [	'Fitted Bkg Function',
									'Num. Injected Signal',
									'Signal Mass',
									'Mean GP Spurious Signal',
									'Std. Dev. GP Spurious Signal',
									'Mean Raw Spurious Signal',
									'Std. Dev. Raw Spurious Signal',
									'Mean GP Delta(S)',
									'Std. Dev. GP Delta(S)',
									'Mean Raw Delta(S)',
									'Std. Dev. Raw Delta(S)',
									'Bias Significance' ] )

			csv_writer.writerow([])

			print(" ---------------------- ")
			print(" ---------------------- ")

			for ifunc,funcDef in enumerate(myBkgFunctions):

				testFunction = funcDef[0]

				print( " --- Test Background Function: " , testFunction , " --- " )
				if( nSigInject > 0 ):
					print("   Mean Spurious Signal (Toy) - Injected Signal: " , np.mean(toySpurSig[ifunc]) , " +/- " , np.std(toySpurSig[ifunc]) )
					print("   Mean Spurious Signal (GP) - Injected Signal: " , np.mean(gprSpurSig[ifunc]) , " +/- " , np.std(gprSpurSig[ifunc]) )
					print("         (S+B Fit) Delta_S (Toy): " , np.mean(toyDeltaS[ifunc]) , " +/- " , np.std(toyDeltaS[ifunc])  )
					print("         (S+B Fit) Delta_S (GP): " , np.mean(gprDeltaS[ifunc]) , " +/- " , np.std(gprDeltaS[ifunc])  )
					print("         Injected Signal = " , nSigInject )

				else:
					print("   Mean Spurious Signal (Toy): " , np.mean(toySpurSig[ifunc]) , " +/- " , np.std(toySpurSig[ifunc]) )
					print("   Mean Spurious Signal (GP): " , np.mean(gprSpurSig[ifunc]) , " +/- " , np.std(gprSpurSig[ifunc]) )
					print("         (S+B Fit) Delta_S (Toy): " , np.mean(toyDeltaS[ifunc]) , " +/- " , np.std(toyDeltaS[ifunc])  )
					print("         (S+B Fit) Delta_S (GP): " , np.mean(gprDeltaS[ifunc]) , " +/- " , np.std(gprDeltaS[ifunc])  )

				meanDeltaS = np.mean(toyDeltaS[ifunc])
				if( meanDeltaS == 0.0 ):
					print("WARNING: The fit deltaS values were not saved in these toy tests.")
					print("   The bias significance will be calculated using sigma(Raw Spur. Sig.) instead.")
					meanDeltaS = np.std(toySpurSig[ifunc])

				gpBias = np.abs( np.mean(toySpurSig[ifunc] - gprSpurSig[ifunc]) ) / np.sqrt( np.mean(toySpurSig[ifunc])**2 + meanDeltaS**2 )
				print("            GP Bias Significance = ", gpBias )

				csv_writer.writerow( [	testFunction,
										nSigInject,
										'All',
										np.mean(gprSpurSig[ifunc]),
										np.std(gprSpurSig[ifunc]),
										np.mean(toySpurSig[ifunc]),
										np.std(toySpurSig[ifunc]),
										np.mean(gprDeltaS[ifunc]),
										np.std(gprDeltaS[ifunc]),
										np.mean(toyDeltaS[ifunc]),
										np.std(toyDeltaS[ifunc]),
										gpBias ] )

				print(" -------------------- ")
			print(" -------------------- ")

			csv_writer.writerow([])

			if( hasattr(userin, 'toyMassScanPoints') ):
				nMassPoints = len( userin.toyMassScanPoints )
				if( nMassPoints <= 1 ): continue

				print(" ---------------------- ")
				print(" -- Mass Scan Result -- ")
				print(" ---------------------- ")

				for ifunc,funcDef in enumerate(myBkgFunctions):

					testFunction = funcDef[0]

					print( " --- Test Background Function: " , testFunction , " --- " )
					for imass,mass in enumerate( userin.toyMassScanPoints ):
						print(" --- S-Mass = ",mass)
						if( nSigInject > 0 ):
							print("      Mean Spurious Signal (Toy) - Injected Signal: " , np.mean(toySpurSig_massScan[ifunc][imass]) , " +/- " , np.std(toySpurSig_massScan[ifunc][imass]) )
							print("      Mean Spurious Signal (GP) - Injected Signal: " , np.mean(gprSpurSig_massScan[ifunc][imass]) , " +/- " , np.std(gprSpurSig_massScan[ifunc][imass]) )
							print("         (S+B Fit) Delta_S (Toy): " , np.mean(toyDeltaS_massScan[ifunc][imass]) , " +/- " , np.std(toyDeltaS_massScan[ifunc][imass])  )
							print("         (S+B Fit) Delta_S (GP): " , np.mean(gprDeltaS_massScan[ifunc][imass]) , " +/- " , np.std(gprDeltaS_massScan[ifunc][imass])  )
							print("         Injected Signal = " , nSigInject )
						else:
							print("      Mean Spurious Signal (Toy): " , np.mean(toySpurSig_massScan[ifunc][imass]) , " +/- " , np.std(toySpurSig_massScan[ifunc][imass]) )
							print("      Mean Spurious Signal (GP): " , np.mean(gprSpurSig_massScan[ifunc][imass]) , " +/- " , np.std(gprSpurSig_massScan[ifunc][imass]) )
							print("         (S+B Fit) Delta_S (Toy): " , np.mean(toyDeltaS_massScan[ifunc][imass]) , " +/- " , np.std(toyDeltaS_massScan[ifunc][imass])  )
							print("         (S+B Fit) Delta_S (GP): " , np.mean(gprDeltaS_massScan[ifunc][imass]) , " +/- " , np.std(gprDeltaS_massScan[ifunc][imass])  )

						meanDeltaS = np.mean(toyDeltaS_massScan[ifunc][imass])
						if( meanDeltaS == 0.0 ):
							print("WARNING: The fit deltaS values were not saved in these toy tests.")
							print("   The bias significance will be calculated using sigma(Raw Spur. Sig.) instead.")
							meanDeltaS = np.std(toySpurSig_massScan[ifunc][imass])
						gpBias = np.abs( np.mean( toySpurSig_massScan[ifunc][imass] - gprSpurSig_massScan[ifunc][imass] ) ) / np.sqrt( np.mean(toySpurSig_massScan[ifunc][imass])**2 + meanDeltaS**2 )
						print("            GP Bias Significance = ", gpBias )

						csv_writer.writerow( [	testFunction,
												nSigInject,
												mass,
												np.mean(gprSpurSig_massScan[ifunc][imass]),
												np.std(gprSpurSig_massScan[ifunc][imass]),
												np.mean(toySpurSig_massScan[ifunc][imass]),
												np.std(toySpurSig_massScan[ifunc][imass]),
												np.mean(gprDeltaS_massScan[ifunc][imass]),
												np.std(gprDeltaS_massScan[ifunc][imass]),
												np.mean(toySpurSig_massScan[ifunc][imass]),
												np.std(toySpurSig_massScan[ifunc][imass]),
												gpBias ] )

						print(" -------------------- ")
				print(" -------------------- ")



		### Need to close some canvases to avoid weird ROOT issues

		canvas_avgFitShape.Close()
		canvas_spurSigDist.Close()
		canvas_allGPfits.Close()
		canvas_SBfits.Close()
		canvas_GPLengthScale.Close()


		### Also delete some histograms

		del avgGPhisto
		del avgToyhisto
		del residGPhisto
		del residToyhisto





if __name__== "__main__":
	main()
