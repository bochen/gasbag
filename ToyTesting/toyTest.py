import numpy as np
from scipy.optimize import curve_fit
import ROOT 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel 

import sys 
sys.path.append('inc/')
from FallExpKernel import * 
from LinearErrorKernel import * 


########### User Defined Options ##########

# These are all pulled from the input file below: 

from inputs_ToyTest import * 

###########################################	

### Basic Exponential Implementation for Creating GPR Prior 

def mySimpleExponential(x, a, b):
	return a*np.exp( b * x ) 	

###########################################	

### A quick function to create histograms from numpy arrays 

def makeHistoFromArray( inputArray , outputHisto , inputError = np.array('i') ): 
	nBins = outputHisto.GetNbinsX() 
	if( len(inputArray) != nBins ): 
		print("WARNING in makeHistoFromArray")
		print("Number of bins in histogram does not match length of input array!")
		print("Returning empty histogram.") 
		return 
	for iBin in range(nBins): 
		outputHisto.SetBinContent( iBin+1 , inputArray[iBin] ) 
		if( inputError.shape ): outputHisto.SetBinError( iBin+1 , inputError[iBin] ) 

###########################################

### A quick function to create numpy arrays from histograms

def makeArrayFromHisto( inputHisto ): 
	nBins = inputHisto.GetNbinsX()
	outputArray = np.zeros( nBins , 'd' )
	for iBin in range(nBins): 
		outputArray[iBin] = inputHisto.GetBinContent( iBin+1 ) 
	return outputArray

###########################################

### Generate the workspace (signal+background) ###

def generateWorkspace( wsName , whichFunction , range_Myy ):

	nBkgEvts = int(nEvts)
	ws = ROOT.RooWorkspace(wsName)
	ws.factory('rooMyy[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	ws.factory('RooM0[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	ws.obj('RooM0').setVal(125)
	
	### Generate bkg pdf
	
	# PowerLaw 
	if( whichFunction == "PowerLaw" ): 
		bkgStr = "EXPR:bkg('rooMyy**PowerLaw_b',{rooMyy,PowerLaw_b[-1.447,-20,20]})"
	# Exponential 
	elif( whichFunction == "Exponential" ): 	
		bkgStr = "RooExponential:bkg(rooMyy, Exponential_b[-0.02, -0.5, 0.0])"
	# ExpPoly2 
	elif( whichFunction == "ExpPoly2" ): 	
		bkgStr = "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly2_b+ExpPoly2_c*(rooMyy-100)/100))',{rooMyy,ExpPoly2_b[-2.0,-10,10],ExpPoly2_c[1.0,-10,10]})"
	# ExpPoly3 
	elif( whichFunction == "ExpPoly3" ):
		bkgStr = "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly3_b+ExpPoly3_c*(rooMyy-100)/100+ExpPoly3_d*(rooMyy-100)*(rooMyy-100)/(100*100)))',{rooMyy,ExpPoly3_b[-3.7,-10,10],ExpPoly3_c[2.0,-10,10],ExpPoly3_d[0.5,-10,10]})"
	# Bern 3 
	elif( whichFunction == "Bern3" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern3_b[5,-10,10], Bern3_c[2,-10,10], Bern3_d[2,-10,10], 1})"
	# Bern 4 
	elif( whichFunction == "Bern4" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern4_b[9,-15,15], Bern4_c[4,-10,10], Bern4_d[3,-10,10], Bern4_e[2,-10,10], 1})"
	# Bern 4 
	elif( whichFunction == "Bern5" ): 
		bkgStr = "RooBernstein:bkg(rooMyy, {Bern5_b[10,-15,15], Bern5_c[7.5,-10,10], Bern5_d[5.5,-10,10], Bern5_e[3,-10,10], Bern5_f[2,-10,10], 1})"
	else: 
		print( "Error - Improper input function specified" ) 
		print( "   Options = PowerLaw , Exponential , ExpPoly2 , ExpPoly3 , Bern3 , Bern4 , Bern5" ) 
		
	### Generate sig pdf
	
	# Crystal Ball 
	sigStr = "RooCBShape:sig(rooMyy,RooM0,2,5,10)"
	# Gaussian 
	#sigStr = "RooGaussian:sig(rooMyy,RooM0,2)"

	bkgPDF = ws.factory(bkgStr)		
	sigPDF = ws.factory(sigStr)
	
	nSig_str = "nSig[10,-"+str(nBkgEvts/10)+","+str(nBkgEvts/10)+"]"
	nBkg_str = "nBkg["+str(nBkgEvts)+",0,"+str(5*nBkgEvts)+"]"
	
	ws.factory(nSig_str)
	ws.factory(nBkg_str)
	
	pdfSBstr = "SUM:sigbkgPDF( nSig * sig , nBkg * bkg )"
	sigbkgPDF = ws.factory(pdfSBstr) 
	
	ws.saveSnapshot("initial",ws.allVars())
	
	return ws 


### Create the Toy Template ### 

def makeToy( nBins , range_Myy , nEvts , w_Myy ): 

	binWidth = 1.0*(range_Myy[1]-range_Myy[0])/nBins
	toyTemplateName = "Template"
	toyTemplate = ROOT.TH1F( toyTemplateName , toyTemplateName , nBins , range_Myy[0] , range_Myy[1] ) 

	### Make x-range 

	x_Myy_dense = np.array( [ range_Myy[0]+i*0.01*binWidth for i in range(100*nBins) ] ) 
	rooMyy = ROOT.RooRealVar("M_yy","M_yy",range_Myy[0],range_Myy[1])
	
	### Make Toy Data 
	 
	y_Myy = np.random.choice( x_Myy_dense , nEvts , True , p = w_Myy )
	for entry in y_Myy: 
		toyTemplate.Fill( entry )
	
	return( toyTemplate )


### Create the GPR-Smoothed Template ### 

def makeGPRtemplate( nBins , range_Myy , toyTemplate ): 
	
	x_Myy = np.array( [ range_Myy[0]+1.0*i*(range_Myy[1]-range_Myy[0])/nBins for i in range(nBins) ] )

	### Make array from the toy template 
	
	myyHistArray = makeArrayFromHisto( toyTemplate ) 
	myyErrs = [] 
	for ibin in range( nBins ): 
		myyErrs.append( toyTemplate.GetBinError( ibin+1 ) )
	myyErrs = np.array( myyErrs ) 
	
	### Assume Linear Prior 
	
	#baseline_slope_est = ( np.max(myyHistArray) - np.min(myyHistArray) ) / (x_Myy[0]-x_Myy[-1])
	#myBaseLine = np.array([ np.max(myyHistArray) + baseline_slope_est*(x-x_Myy[0]) for x in x_Myy ])

	### Assume Exponential Prior 
	
	optimal_pars, par_cov = curve_fit(mySimpleExponential, x_Myy, myyHistArray, p0=(myyHistArray[0],-0.02))
	myBaseLine = np.array(mySimpleExponential(x_Myy,optimal_pars[0],optimal_pars[1]))

	### Generate Kernel 

	# Set bounds on the scaling of the GPR template (basically a maximum y-range) 

	const_low = 10.0**-5
	const_hi = nEvts * 10.0

	# Set bounds on the magnitude of the GPR error bars 

	errConst0 = np.max(myyErrs[0])
	errConst_low = 0.90*np.max(myyErrs[0])
	errConst_hi = 1.10*np.max(myyErrs[0])	

	# Estimate the (decreasing) slope of the magnitude of the GPR error bars 

	slope_est0 = -1.0*(myyErrs[0]-myyErrs[-1])/(x_Myy[0]-x_Myy[-1])
	if( slope_est0 < 10**-5 ): slope_est0 = 10**-4
	slope_est_low = 0.5*slope_est0
	slope_est_hi = 1.05*slope_est0

	kernel = ( ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi)) * 
				Gibbs(l0=Gibbs_l0,l0_bounds=(Gibbs_l0_bounds[0],Gibbs_l0_bounds[1]),l_slope=Gibbs_l_slope,l_slope_bounds=(Gibbs_l_slope_bounds[0],Gibbs_l_slope_bounds[1])) +
			   ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
				LinearNoiseKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi),b=slope_est0,b_bounds=(slope_est_low,slope_est_hi)) )

	gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 ) 
	gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
	
	# Check to see if GPR Prediction is effectively the same as the exponential prior 
	# If so, re-do the GPR fitting, but with a flat prior instead of exponential prior 
	
	gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )
	
	if( np.sqrt( (gprPrediction*gprPrediction).sum() )/ myyHistArray.sum() < 0.0001 ): 
		myBaseLine = np.zeros( len(myyHistArray) ) 
		gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 , normalize_y=True ) 
		gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )	
		gprPrediction, gprCov = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_cov=True )

	# Use the GPR template's errors 
	gprError = np.diagonal(gprCov)
	gprPrediction = gprPrediction + myBaseLine
	gprPredictionHisto = ROOT.TH1F( "gprPredictionHisto" , "gprPredictionHisto" , nBins , range_Myy[0] , range_Myy[1] ) 
	makeHistoFromArray( gprPrediction , gprPredictionHisto , gprError )

	# Assign Poisson errors 
# 	gprPrediction = gpr.predict( x_Myy.reshape(len(x_Myy),1) )
# 	gprPrediction = gprPrediction + myBaseLine
# 	gprPredHisto = ROOT.TH1F( "gprPredHisto" , "gprPredHisto" , nBins , range_Myy[0] , range_Myy[1] ) 
# 	makeHistoFromArray( gprPrediction , gprPredHisto )
	
	return( gprPredictionHisto , gpr )


### Make RooPlot for Debugging 

def makeRooPlot( templateRooHist , myTempWS ): 
	
	pdfBkgName = myTempWS.GetName()[9:]
	nBkgEvts = templateRooHist.sumEntries()

	can_sbFit = ROOT.TCanvas("can_sbFit","can_sbFit",600,600)
	can_sbFit.cd() 
	frame = myTempWS.obj('rooMyy').frame() 
	leg = ROOT.TLegend(0.55,0.70,0.89,0.89)
	leg.SetFillStyle(-1) 
	leg.SetBorderSize(0)
	
	templateRooHist.plotOn( frame , ROOT.RooFit.MarkerColor(1) )
	
	myTempWS.obj('sigbkgPDF').plotOn( frame , ROOT.RooFit.LineColor(417) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.LineStyle(9) , ROOT.RooFit.Name('SBfit') ) 
	#myTempWS.obj('sigbkgPDF').plotOn( frame , ROOT.RooFit.Components("pdfBkg"+pdfBkgName) , ROOT.RooFit.LineColor(419) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit') ) 
	myTempWS.obj('bkg').plotOn( frame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit')) 
		
	frame.SetMaximum( 1.3*frame.GetMaximum() )
	frame.Draw() 
	frame.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
	ROOT.ATLAS_LABEL(0.18,0.85)
	ROOT.myText(0.35,0.85,1,"Internal")
	ROOT.myText(0.20,0.79,1,"#splitline{Gen. Function: "+whichFunction+"}{Fit Function: "+pdfBkgName+"}",0.03)
	ROOT.myText(0.20,0.75,1,"N_{events} = "+str(int(nBkgEvts)),0.030)
	
	leg.AddEntry(frame.findObject("h_templateRooHist"),"Fitted Template","ep")
	leg.AddEntry(frame.findObject("bkgOnlyFit"),"Background-Only Fit","l")
	leg.AddEntry(frame.findObject("SBfit"),"Signal+Background Fit","l")

	leg.Draw("same")
	can_sbFit.Update() 
	import pdb; pdb.set_trace() 


### Perform one single fit (S+B) ###

def fitToy( toyTemplate , myTempWS , sig_mass ) :
	
	templateRooHist = ROOT.RooDataHist("templateRooHist", "templateRooHist", ROOT.RooArgList(myTempWS.obj("rooMyy")), toyTemplate) 
	nBkgEvts = toyTemplate.Integral()
	
	### Reset the workspace 
	
	myTempWS.loadSnapshot("initial")
	myTempWS.obj('nBkg').setVal(nBkgEvts)
	myTempWS.obj('nSig').setVal(0)
	myTempWS.obj('RooM0').setVal(sig_mass)
	myTempWS.obj('RooM0').setConstant(ROOT.kTRUE)
	
	### Make the fitter and fit the toy
	
	nll = myTempWS.obj('sigbkgPDF').createNLL(templateRooHist)
	fitter = ROOT.RooMinimizer(nll)
	fitter.setEps(1E-3 / 0.001)
	fitter.setStrategy(1)
	fitter.setPrintLevel(-2)
	fitter.setOffsetting(True)
	fitter.minimize("Minuit2")
	fitResultSigBkg = fitter.save("fitResultSigBkg", "fitResultSigBkg")
	
	spurSigVal = myTempWS.obj('nSig').getVal()
	
	parShortNames = [ "b" , "c" , "d" , "e" , "f" ] 
	bkgFitParams = []
	for par in parShortNames: 
		parName = myTempWS.GetName()[9:] + "_" + par 
		par_exists = myTempWS.var(parName)
		if( not not par_exists ): bkgFitParams.append( myTempWS.var(parName).getValV() ) 
		else: bkgFitParams.append( 0.0 )
		
	#makeRooPlot( templateRooHist , myTempWS )
	
	return( spurSigVal , fitResultSigBkg.status() , bkgFitParams )	


###########################################
########### The Important Stuff ###########

nCategories = len(templateHistoNames) 
		
if ( len(categoryNames) != nCategories ): 
	print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
	categoryNames = templateHistoNames 

if( not not hyperParFileName ): hyperParFile = ROOT.TFile.Open(hyperParFileName)

outFile = ROOT.TFile(outFileName,"RECREATE") 
inFile = ROOT.TFile.Open(inFileName,"READ")
ROOT.gROOT.LoadMacro("inc/PythonPlottingScripts/AtlasStyle.C") 
ROOT.gROOT.LoadMacro("inc/PythonPlottingScripts/AtlasUtils.C") 
ROOT.SetAtlasStyle()

for iCat in range(nCategories): 

	### Set the input parameters 
	
	inFile.cd() 
	inHistName = templateHistoNames[ iCat ] 
	catName = categoryNames[ iCat ] 

	### Import the input histogram

	inHisto = inFile.Get(inHistName)
	inHisto.Rebin( nRebin )
	outFile.cd()

	nBins = inHisto.GetNbinsX()
	xMin = inHisto.GetBinLowEdge(1)
	xMax = inHisto.GetBinLowEdge(nBins+1)
	range_Myy = [ xMin , xMax ]
	x_Myy = np.array( [ xMin+1.0*i*(xMax-xMin)/nBins for i in range(nBins) ] )
	binWidth = 1.0*(xMax-xMin)/nBins

	mcErrs = [] 
	for i in range(nBins): 
		mcErrs.append( inHisto.GetBinError(i+1) )
	mcErrs = np.array(mcErrs)	
	
	nEvts = int(inHisto.GetEffectiveEntries())
	
	if( not not hyperParFileName ): 
		hyperParTree = hyperParFile.Get( "HyperParArea_"+inHistName )
		hyperParTree.GetEntry(0)
		Gibbs_l0_bounds[0] =  hyperParTree.GetLeaf("length_scale_min").GetValue() 
		Gibbs_l0_bounds[1] =  hyperParTree.GetLeaf("length_scale_max").GetValue()
		Gibbs_l_slope_bounds[0] =  hyperParTree.GetLeaf("length_scale_slope_min").GetValue()
		Gibbs_l_slope_bounds[1] =  hyperParTree.GetLeaf("length_scale_slope_max").GetValue()

	Gibbs_l0 = Gibbs_l0_bounds[0] + 0.9*( Gibbs_l0_bounds[1] - Gibbs_l0_bounds[0] )
	Gibbs_l_slope = Gibbs_l_slope_bounds[0] + 0.9*( Gibbs_l_slope_bounds[1] - Gibbs_l_slope_bounds[0] )

	### Determine the Optimal Basis Analytical Function to Use 

	allFunctions = [ "PowerLaw" , "Exponential" , "ExpPoly2" , "ExpPoly3" , "Bern3" , "Bern4" , "Bern5" ]
	nDOF = []
	allChiSqrd = []
	allChiSqrd2 = [] 
	for i,testFunction in enumerate(allFunctions): 
		testBkgWS = generateWorkspace( "test"+testFunction+"WS" , testFunction , range_Myy ) 
		testerRooHist = ROOT.RooDataHist("testerRooHist", "testerRooHist", ROOT.RooArgList(testBkgWS.obj("rooMyy")), inHisto)
		testBkgWS.pdf("bkg").fitTo( testerRooHist , ROOT.RooFit.SumW2Error(ROOT.kTRUE)) 
		testFrame = testBkgWS.obj('rooMyy').frame() 
		testerRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(1) )
		testBkgWS.pdf('bkg').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit')) 		
		nDOF.append( testBkgWS.pdf('bkg').getVariables().getSize() )
		allChiSqrd.append( testFrame.chiSquare(nDOF[i]) )
			
	nDOF = np.array(nDOF,'i')	
	allChiSqrd = np.array(allChiSqrd)
	#bestFunctionNum = np.where( allChiSqrd == allChiSqrd[allChiSqrd>=1.0].min() )[0][0]
	bestFunctionNum = np.where( allChiSqrd == allChiSqrd.min() )[0][0]
	whichFunction = allFunctions[bestFunctionNum]
	
	### Define the weight function from which to generate the toy template 

	w_Bkg = np.zeros( nBins*100 )
	w_Sig = np.zeros( nBins*100 )

	basisWS = generateWorkspace( "basisWS" , whichFunction , range_Myy ) 
	h_basisBkgPDF = basisWS.obj("bkg").createHistogram("h_basisBkgPDF",basisWS.obj("rooMyy"),ROOT.RooFit.Binning(nBins*100,range_Myy[0],range_Myy[1]))
	w_Bkg = makeArrayFromHisto( h_basisBkgPDF ) 
	h_basisSigPDF = basisWS.obj("sig").createHistogram("h_basisSigPDF",basisWS.obj("rooMyy"),ROOT.RooFit.Binning(nBins*100,range_Myy[0],range_Myy[1]))
	w_Sig = makeArrayFromHisto( h_basisSigPDF ) 

	w_Bkg = w_Bkg / w_Bkg.sum()
	w_Sig = w_Sig / w_Sig.sum()

	### Make the "tester" workspaces 

	myTempWorkspaces = []

	for ibkg,pdfBkgName in enumerate(myPDFs): 
		tempWSname = "testerWS_"+pdfBkgName
		myTempWorkspaces.append( generateWorkspace( tempWSname , pdfBkgName , range_Myy ) )
	 
	### Make ROOT trees to hold the outputs (one per background function) 

	outputTrees = []
	b_nBkgEvts = []
	b_nSigEvts = []
	b_sigMass = []
	b_spurSigGPRValue = []
	b_spurSigToyValue = []
	b_spurSigGPRValue_abs = []
	b_spurSigToyValue_abs = []
	b_GPR_fitStatus = []
	b_Toy_fitStatus = []

	b_GPR_HyperPar_Gibbs_l0 = []
	b_GPR_HyperPar_Gibbs_l_slope = []
	b_GPR_HyperPar_Noise_lvl = []
	b_GPR_HyperPar_Noise_b = []

	b_GPR_bkgFitPars = [] 
	b_Toy_bkgFitPars = [] 

	for ibkg,pdfBkgName in enumerate(myPDFs): 

		treeName = inHistName+"_bkg_"+pdfBkgName
		outputTrees.append( ROOT.TTree(treeName,treeName) )

		b_nBkgEvts.append( np.zeros(1, dtype=float) )
		b_nSigEvts.append( np.zeros(1, dtype=float) )
		b_sigMass.append( np.zeros(1, dtype=float) )
		b_spurSigGPRValue.append( np.zeros(1, dtype=float) )
		b_spurSigToyValue.append( np.zeros(1, dtype=float) )
		b_spurSigGPRValue_abs.append( np.zeros(1, dtype=float) )
		b_spurSigToyValue_abs.append( np.zeros(1, dtype=float) )
		b_GPR_fitStatus.append( np.zeros(1, dtype=float) )
		b_Toy_fitStatus.append( np.zeros(1, dtype=float) )
		b_GPR_HyperPar_Gibbs_l0.append( np.zeros(1, dtype='d') )
		b_GPR_HyperPar_Gibbs_l_slope.append( np.zeros(1, dtype='d') )
		b_GPR_HyperPar_Noise_lvl.append( np.zeros(1, dtype='d') ) 
		b_GPR_HyperPar_Noise_b.append( np.zeros(1, dtype='d') )
		b_GPR_bkgFitPars.append( np.zeros(5, dtype='d') )
		b_Toy_bkgFitPars.append( np.zeros(5, dtype='d') )
	
		outputTrees[-1].Branch("nBkgEvts",b_nBkgEvts[-1],"nBkgEvts/D")
		outputTrees[-1].Branch("nSigEvts",b_nSigEvts[-1],"nSigEvts/D")
		outputTrees[-1].Branch("sigMass",b_sigMass[-1],"sigMass/D")
		outputTrees[-1].Branch("nSpurSigValue_GPR",b_spurSigGPRValue[-1],"nSpurSigValue_GPR/D")
		outputTrees[-1].Branch("nSpurSigValue_Toy",b_spurSigToyValue[-1],"nSpurSigValue_Toy/D")
		outputTrees[-1].Branch("nSpurSigAbsValue_GPR",b_spurSigGPRValue_abs[-1],"nSpurSigAbsValue_GPR/D")
		outputTrees[-1].Branch("nSpurSigAbsValue_Toy",b_spurSigToyValue_abs[-1],"nSpurSigAbsValue_Toy/D")
		outputTrees[-1].Branch("GPR_fitStatus",b_GPR_fitStatus[-1],"GPR_fitStatus/D")
		outputTrees[-1].Branch("Toy_fitStatus",b_Toy_fitStatus[-1],"Toy_fitStatus/D")
	
		outputTrees[-1].Branch("GPR_HyperPar_Gibbs_l0",b_GPR_HyperPar_Gibbs_l0[-1],"b_GPR_HyperPar_Gibbs_l0/D")
		outputTrees[-1].Branch("GPR_HyperPar_Gibbs_l_slope",b_GPR_HyperPar_Gibbs_l_slope[-1],"b_GPR_HyperPar_Gibbs_l_slope/D")
		outputTrees[-1].Branch("GPR_HyperPar_Noise_lvl",b_GPR_HyperPar_Noise_lvl[-1],"b_GPR_HyperPar_Noise_lvl/D")
		outputTrees[-1].Branch("GPR_HyperPar_Noise_b",b_GPR_HyperPar_Noise_b[-1],"b_GPR_HyperPar_Noise_b/D")
	
		outputTrees[-1].Branch("GPR_bkgFitPars",b_GPR_bkgFitPars[-1],"b_GPR_bkgFitPars[5]/D")
		outputTrees[-1].Branch("Toy_bkgFitPars",b_Toy_bkgFitPars[-1],"b_Toy_bkgFitPars[5]/D")
	

	### Run the Tests and Store the Spurious Signal Values ###

	for itoy in range( nToys ): 
		myToy = makeToy( nBins , range_Myy , nEvts , w_Bkg )
		# add in a real signal shape 
		mySignalToy = makeToy( nBins , range_Myy , int(sigFrac*nEvts) , w_Sig ) 
		myToy.Add(mySignalToy)
		myToy.Scale( (1.0+sigFrac)*inHisto.Integral() / myToy.Integral() )

		myGPRtemplate , gprFit = makeGPRtemplate( nBins , range_Myy , myToy )
		params = gprFit.kernel_.get_params()
			
# 		ct = ROOT.TCanvas('ct','ct',600,600) 
# 		inHisto.Draw("ep")
# 		myToy.SetLineColor(2)
# 		myToy.SetMarkerColor(2)
# 		myToy.Draw("ep same")
# 		myGPRtemplate.SetMarkerSize(0)
# 		myGPRtemplate.SetLineColor(4)
# 		myGPRtemplate.Draw("ehist same")
# 		import pdb; pdb.set_trace() 

		for ibkg,pdfBkgName in enumerate(myPDFs): 
	
			# save the GPR hyper-parameters to output 
			b_GPR_HyperPar_Gibbs_l0[ibkg][0] = params['k1__k2__l0']
			b_GPR_HyperPar_Gibbs_l_slope[ibkg][0] = params['k1__k2__l_slope']
			b_GPR_HyperPar_Noise_lvl[ibkg][0] = params['k2__k2__noise_level']
			b_GPR_HyperPar_Noise_b[ibkg][0] = params['k2__k2__b']
		
			for sig_mass in masses: 
			
				myTempWS = myTempWorkspaces[ibkg]
								
				spurSigValues_Toy = fitToy( myToy , myTempWS , sig_mass ) 
				spurSigValues_GPR = fitToy( myGPRtemplate , myTempWS , sig_mass ) 
			
				b_nBkgEvts[ibkg][0] = nEvts
				b_nSigEvts[ibkg][0] = int(sigFrac*nEvts)
				b_sigMass[ibkg][0] = sig_mass
				b_spurSigGPRValue[ibkg][0] = spurSigValues_GPR[0] 
				b_spurSigGPRValue_abs[ibkg][0] = np.abs( spurSigValues_GPR[0] )
				b_spurSigToyValue[ibkg][0] = spurSigValues_Toy[0]
				b_spurSigToyValue_abs[ibkg][0] = np.abs( spurSigValues_Toy[0] )	
				b_GPR_fitStatus[ibkg][0] = spurSigValues_GPR[1]
				b_Toy_fitStatus[ibkg][0] = spurSigValues_Toy[1]		
			
				# fill the fit parameters from the bkg fit to the toys 
				holdToyFitParams = spurSigValues_Toy[2]
				holdGPRFitParams = spurSigValues_GPR[2]
				for ipar in range(len(spurSigValues_GPR)):
					b_GPR_bkgFitPars[ibkg][ipar] = holdGPRFitParams[ipar]
					b_Toy_bkgFitPars[ibkg][ipar] = holdToyFitParams[ipar]
			
				outputTrees[ibkg].Fill() 	


		### Save the Trees to a ROOT file 

		outFile.cd() 
		for tree in outputTrees:
			tree.Write() 

print( "All done!" )