import ROOT 
import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../PythonPlottingScripts/")

########### User Defined Options ##########

# These are all pulled from the input file below: 

from inputs_ToyTestMC import * 

###########################################

ROOT.gROOT.LoadMacro("../inc/PythonPlottingScripts/AtlasStyle.C") 
ROOT.gROOT.LoadMacro("../inc/PythonPlottingScripts/AtlasUtils.C") 
ROOT.SetAtlasStyle()

inFile = ROOT.TFile.Open( combinedToyTestFileName ) 
sigMass = 125

fitFunctions = myPDFs
nStatsLevels = len(statistics)

gprMeans = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
gprRMSs = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
gprLeftWidths = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
gprRightWidths = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )

toyMeans = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
toyRMSs = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
toyLeftWidths = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )
toyRightWidths = np.zeros( (nStatsLevels,len(fitFunctions)) , 'd' )

gprHistos = [[0]*len(fitFunctions) for i in range(nStatsLevels)]
toyHistos = [[0]*len(fitFunctions) for i in range(nStatsLevels)]
gprHistoGaussFits = [[0]*len(fitFunctions) for i in range(nStatsLevels)]
toyHistoGaussFits = [[0]*len(fitFunctions) for i in range(nStatsLevels)]


### Start by plotting the Spurious Signal values for a given signal mass (as set from the input file) 

distributionCanvases = [] 

leg = ROOT.TLegend(0.70,0.7,0.89,0.80)
leg.SetFillStyle(-1)
leg.SetBorderSize(0)

for iEvt,nEvts in enumerate(statistics):

	dummyCan = ROOT.TCanvas("dummyCan","dummyCan",600,600)
	dummyCan.cd()

	for ifunc,func in enumerate( fitFunctions ): 
	
		treeName = "InclusiveToyTest_bkg_"+str(func) 
		inTree = inFile.Get( treeName )
		
		if (sigMass==-1): cut = "((Toy_fitStatus==0||Toy_fitStatus==1)&&nBkgEvts=="+str(nEvts)+")"
		else: cut = "((Toy_fitStatus==0||Toy_fitStatus==1)&&nBkgEvts=="+str(nEvts)+"&&sigMass=="+str(sigMass)+")"  
	
		gprHistoName = "h_GPR_fit_"+str(func)+"_nEvts_"+str(nEvts)
		gprHistos[iEvt][ifunc] = ROOT.TH1D(gprHistoName,gprHistoName,100000000,-1,1) 

		toyHistoName = "h_Toy_fit_"+str(func)+"_nEvts_"+str(nEvts)
		toyHistos[iEvt][ifunc] = ROOT.TH1D(toyHistoName,toyHistoName,100000000,-1,1) 

		inTree.Draw( "nSpurSigValue_GPR/sqrt(nBkgEvts)>>"+gprHistoName , cut )
		inTree.Draw( "nSpurSigValue_Toy/sqrt(nBkgEvts)>>"+toyHistoName , cut )
	
		smax = toyHistos[iEvt][ifunc].FindLastBinAbove() 
		smin = toyHistos[iEvt][ifunc].FindFirstBinAbove() 
		#smax = gprHistos[iEvt][ifunc].FindLastBinAbove() 
		#smin = gprHistos[iEvt][ifunc].FindFirstBinAbove() 
		nbins = np.int(np.ceil( (smax-smin)/100. ))
		if( nbins < 1 ): nbins = 1 
		gprHistos[iEvt][ifunc].Rebin(nbins)
		toyHistos[iEvt][ifunc].Rebin(nbins)
		
	
	dummyCan.Close() 

	distributionCanvases.append( ROOT.TCanvas("Canvas_"+str(nEvts),"Canvas_"+str(nEvts),1300,600) )
	distributionCanvases[-1].Divide(4,2)

	#Make distribution plots  
	for ifunc,func in enumerate(fitFunctions):
	
		distributionCanvases[-1].GetPad(ifunc+1).cd()
		
		spurMin = toyHistos[iEvt][ifunc].GetMean() - 3*toyHistos[iEvt][ifunc].GetRMS()
		spurMax = toyHistos[iEvt][ifunc].GetMean() + 3*toyHistos[iEvt][ifunc].GetRMS()
		
		toyHistos[iEvt][ifunc].SetLineColor(2)
		toyHistos[iEvt][ifunc].SetLineWidth(2)
		toyHistos[iEvt][ifunc].SetMarkerSize(0)
		#toyHistos[iEvt][ifunc].GetXaxis().SetRangeUser( spurMin , spurMax )
		toyHistos[iEvt][ifunc].GetXaxis().SetRange( toyHistos[iEvt][ifunc].FindBin(spurMin) , toyHistos[iEvt][ifunc].FindBin(spurMax) )
		#toyHistos[iEvt][ifunc].GetXaxis().SetRange( toyHistos[iEvt][ifunc].FindFirstBinAbove(0.0) , toyHistos[iEvt][ifunc].FindLastBinAbove(0.0) )
		toyHistos[iEvt][ifunc].SetMaximum( 1.2*gprHistos[iEvt][ifunc].GetMaximum() )
		toyHistos[iEvt][ifunc].GetXaxis().SetTitle("N_{spur sig} / #sqrt{N_{eff events}}")
		toyHistos[iEvt][ifunc].GetYaxis().SetTitle("N_{Toys}")
		toyHistos[iEvt][ifunc].Draw("ehist")

		gprHistos[iEvt][ifunc].SetLineColor(4)
		gprHistos[iEvt][ifunc].SetLineWidth(2)
		gprHistos[iEvt][ifunc].SetMarkerSize(0)
		gprHistos[iEvt][ifunc].GetXaxis().SetRange( gprHistos[iEvt][ifunc].FindBin(spurMin) , gprHistos[iEvt][ifunc].FindBin(spurMax) )
		gprHistos[iEvt][ifunc].GetXaxis().SetTitle("N_{spur sig} / #sqrt{N_{eff events}}")
		gprHistos[iEvt][ifunc].GetYaxis().SetTitle("N_{Toys}" )
		gprHistos[iEvt][ifunc].Draw("ehist same")
		
		
		gprFitName = "gaussGPR_fit_"+str(func)+"_nEvts_"+str(nEvts)
		toyFitName = "gaussToy_fit_"+str(func)+"_nEvts_"+str(nEvts)
		
		gprHistoGaussFits[iEvt][ifunc] = ROOT.TF1(gprFitName,"[2]*TMath::Gaus(x,[0],[1])",-1,1) 
		toyHistoGaussFits[iEvt][ifunc] = ROOT.TF1(toyFitName,"[2]*TMath::Gaus(x,[0],[1])",-1,1) 

		gprHistoGaussFits[iEvt][ifunc].SetParLimits(-1,0,1)
		gprHistoGaussFits[iEvt][ifunc].SetParLimits(1,0,1)
		gprHistoGaussFits[iEvt][ifunc].SetParameters(gprHistos[iEvt][ifunc].GetMean(),gprHistos[iEvt][ifunc].GetRMS());

		toyHistoGaussFits[iEvt][ifunc].SetParLimits(-1,0,1)
		toyHistoGaussFits[iEvt][ifunc].SetParLimits(1,0,1)
		toyHistoGaussFits[iEvt][ifunc].SetParameters(toyHistos[iEvt][ifunc].GetMean(),toyHistos[iEvt][ifunc].GetRMS());

		gprHistoGaussFits[iEvt][ifunc].SetLineColor(860)
		toyHistoGaussFits[iEvt][ifunc].SetLineColor(632)
		gprHistoGaussFits[iEvt][ifunc].SetLineStyle(9)
		toyHistoGaussFits[iEvt][ifunc].SetLineStyle(9)
		gprHistoGaussFits[iEvt][ifunc].SetLineWidth(2)
		toyHistoGaussFits[iEvt][ifunc].SetLineWidth(2)
		gprHistos[iEvt][ifunc].Fit(gprFitName,"emr") 
		toyHistos[iEvt][ifunc].Fit(toyFitName,"emr same") 
		gprHistos[iEvt][ifunc].DrawCopy()
		toyHistos[iEvt][ifunc].DrawCopy("same")	
		
		if( ifunc == 0 ):
			ROOT.ATLAS_LABEL(0.20,0.85)
			ROOT.myText(0.37,0.85,1,"Internal")
			ROOT.myText(0.20,0.80,1,"GPR Toy Tests",0.04)
			ROOT.myText(0.20,0.75,1,"N_{Events} = "+str(nEvts),0.04)
			
			if( iEvt == 0 ):
				leg.AddEntry( toyHistos[iEvt][ifunc] , "Raw Toy" )
				leg.AddEntry( gprHistos[iEvt][ifunc] , "GPR Toy" )
		
		ROOT.myText(0.7,0.85,1,func,0.04)
		
		ROOT.myText(0.20,0.54,1,"#mu_{raw}="+"{0:.6f}".format(toyHistoGaussFits[iEvt][ifunc].GetParameter(0)),0.04)
		ROOT.myText(0.20,0.50,1,"#mu_{GPR}="+"{0:.6f}".format(gprHistoGaussFits[iEvt][ifunc].GetParameter(0)),0.04)
		
		if( ifunc == 3 ): leg.Draw() 
			
		distributionCanvases[-1].Update()
		distributionCanvases[-1].Print("GPR_ToyTestDistributions_nEvts_"+str(nEvts)+".pdf")
 

### Next, make summary plots of the distributions above 
			
parameterCanvas = ROOT.TCanvas("parCanvas","parCanvas",1300,600) 
parameterCanvas.Divide(4,2)
	
gprParamGraphs = []
toyParamGraphs = []
paramMultiGraphs = [] 

for ifunc,func in enumerate( fitFunctions ): 
	
	gprParamGraphs.append( ROOT.TGraphErrors(nStatsLevels) )
	toyParamGraphs.append( ROOT.TGraphErrors(nStatsLevels) )
	paramMultiGraphs.append( ROOT.TMultiGraph("parMultiGraph_"+func,func) )
	
	for iEvt,nEvts in enumerate(statistics):	
	
		gprParamGraphs[-1].SetPoint( iEvt+1 , nEvts , gprHistoGaussFits[iEvt][ifunc].GetParameter(0) )
		gprParamGraphs[-1].SetPointError( iEvt+1 , 100.0 , gprHistoGaussFits[iEvt][ifunc].GetParameter(1) )
		
		toyParamGraphs[-1].SetPoint( iEvt+1 , nEvts , toyHistoGaussFits[iEvt][ifunc].GetParameter(0) )
		toyParamGraphs[-1].SetPointError( iEvt+1 , 100.0 , toyHistoGaussFits[iEvt][ifunc].GetParameter(1) )
	
	parameterCanvas.GetPad(ifunc+1).cd() 
	parameterCanvas.GetPad(ifunc+1).SetLogx(1)

	gprParamGraphs[-1].SetLineColor(4)
	gprParamGraphs[-1].SetMarkerColor(4)
	gprParamGraphs[-1].SetMarkerSize(1)
	#gprParamGraphs[-1].SetFillStyle(3002)
	gprParamGraphs[-1].SetFillColorAlpha(4,0.3)
	
	toyParamGraphs[-1].SetLineColor(2)
	toyParamGraphs[-1].SetMarkerColor(2)
	toyParamGraphs[-1].SetMarkerSize(1)
	#toyParamGraphs[-1].SetFillStyle(3002)
	toyParamGraphs[-1].SetFillColorAlpha(2,0.3)
	
	paramMultiGraphs[-1].Add( toyParamGraphs[-1] )
	paramMultiGraphs[-1].Add( gprParamGraphs[-1] ) 
	paramMultiGraphs[-1].Draw("alf3")
	paramMultiGraphs[-1].GetXaxis().SetLimits(statistics[0],statistics[-1])
	paramMultiGraphs[-1].Draw("alf3")
	
	if( ifunc == 0 ):
		ROOT.ATLAS_LABEL(0.20,0.85)
		ROOT.myText(0.35,0.85,1,"Internal")
		ROOT.myText(0.20,0.80,1,"GPR Toy Tests",0.04)
		
	if( ifunc == 3 ): leg.Draw() 
	
	ROOT.myText(0.50,0.2,1,"Fit Function: "+func,0.04)
		
	parameterCanvas.Update()

parameterCanvas.Print("GPR_ToyTestSummary.pdf")

### Last, plot the bias (measured by N_sig) for the entire mass range 

truthHistos = []
gprFitHistos = []
biasHistos = []

allSigMasses = np.array( [ xMin+1.0*i*(xMax-xMin)/nBins for i in range(nBins) ] )

treeName = "ToyTests_GPRfitBiasInfo"
biasTree = inFile.Get( treeName )

biasCanvas = ROOT.TCanvas("BiasCanvas","BiasCanvas",1300,600) 
biasCanvas.Divide(3,2)

for iEvt,nEvts in enumerate(statistics):

	dummyCan = ROOT.TCanvas("dummyCan","dummyCan",600,600)
	dummyCan.cd()

	histoName = "truthShape_"+str(nEvts)+"_evts" 
	truthHistos.append( ROOT.TH1F(histoName,histoName,nBins,xMin,xMax) )

	histoName = "gprFitShape_"+str(nEvts)+"_evts" 
	gprFitHistos.append( ROOT.TH1F(histoName,histoName,nBins,xMin,xMax) )

	histoName = "bias_"+str(nEvts)+"_evts" 
	biasHistos.append( ROOT.TH1F(histoName,histoName,nBins,xMin,xMax) )

	for ibin,mass in enumerate(allSigMasses): 
		dummyHisto = ROOT.TH1F("dummyHisto","dummyHisto",int(20*nEvts),-nEvts,nEvts)
		cut = "(nEvts=="+str(nEvts)+"&&Myy=="+str(mass)+")" 

		biasTree.Draw( "scaledTrueValue>>dummyHisto" , cut )
		truthHistos[-1].SetBinContent( ibin+1 , dummyHisto.GetMean() )
		truthHistos[-1].SetBinError( ibin+1 , 0.0 )
		
		biasTree.Draw( "GPRValue>>dummyHisto" , cut )
		gprFitHistos[-1].SetBinContent( ibin+1 , dummyHisto.GetMean() )
		gprFitHistos[-1].SetBinError( ibin+1 , dummyHisto.GetRMS() / np.sqrt( dummyHisto.GetEntries() ) )
		
	biasHistos[iEvt].Add( truthHistos[iEvt] , gprFitHistos[iEvt] , 1.0 , -1.0 )
	biasHistos[iEvt].Divide( truthHistos[iEvt] )

	#Make distribution plots  
	
	biasCanvas.GetPad(iEvt+1).cd()
	
	biasHistos[iEvt].SetLineColor(1)
	biasHistos[iEvt].SetLineWidth(1)
	biasHistos[iEvt].SetMarkerSize(0)
	biasHistos[iEvt].GetXaxis().SetRange( biasHistos[iEvt].FindBin(xMin) , biasHistos[iEvt].FindBin(xMax) )
	biasHistos[iEvt].SetMinimum( -0.1 ) 
	biasHistos[iEvt].SetMaximum( 0.1 ) 
	biasHistos[iEvt].GetXaxis().SetTitle("M_{#gamma#gamma}")
	biasHistos[iEvt].GetYaxis().SetTitle("% Deviation from Truth")
	biasHistos[iEvt].Draw("ehist")

	if( iEvt == 0 ):
		ROOT.ATLAS_LABEL(0.20,0.85)
		ROOT.myText(0.35,0.85,1,"Internal")
		ROOT.myText(0.20,0.80,1,"GPR Toy Tests",0.04)

	ROOT.myText(0.7,0.7,1,"N_{Events} = "+str(nEvts),0.04)
				
biasCanvas.Update()
biasCanvas.Print("Plots_GPR_fitBiases.pdf")	